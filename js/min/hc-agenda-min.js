jQuery(function($) {
txtTitulos();
var pacienteEdit;
var nombrePaciente;
 var mensajeAlerta;
// llenando datos
function txtTitulos (){
	$('.topBar #titulos').empty();
	$('.topBar #titulos').append('Consultas de este més.');
	$('.fc-button-month').html('Mes')
}
function cambiaTitulo () {
	$('.topBar #titulos').empty();
	$('.topBar #titulos').append(nombrePaciente);
	nombrePaciente = '';
}

$('.listaFichas li').click( function(){
	if ( $(this).hasClass('fichaDisabled')) {
		mensajeAlerta = "Guarde su diagnostico antes de seleccionar un nuevo paciente.";
		alertas();

		
	} else {

	$('.listaFichas li').removeClass('fichaSelect');
	$('.listaFichas li').addClass('fichaDisabled');
	$(this).addClass('fichaSelect').removeClass('fichaDisabled');
	console.log(this);
	pacienteEdit = $(this);
	activaEditor()
	}
})

function activaEditor(){
	 $('#calendar').fadeOut(200, function(){
	 	$('.editor').fadeIn(200);
	 	cambiaTitulo();
	 	
	 });
}

$('#cancelar').click( cancelarEditor );

function cancelarEditor(){
		 $('.editor').fadeOut(200, function(){
	 	 $('#calendar').fadeIn(200);
	 	 $(pacienteEdit).removeClass('fichaSelect');
	 	 $('.listaFichas li').removeClass('fichaDisabled');
	 	 txtTitulos();
	 });

}

function alertas(){
	console.log(mensajeAlerta)
	$(".alertas .boton").html('<button>OK</button>');
	$(".alertas button").click( cierraAlerta );
	$(".alertas").fadeIn('fast');
	$(".alertas .mensaje").html(mensajeAlerta);

}

function cierraAlerta(){
		$(".alertas").fadeOut('fast', function(){
				$(".alertas .mensaje").empty('');
			});
		
}


/* initialize the external events
	-----------------------------------------------------------------*/

	$('#external-events div.external-event').each(function() {

		// create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
		// it doesn't need to have a start or end
		var eventObject = {
			title: $.trim($(this).text()) // use the element's text as the event title
		};

		// store the Event Object in the DOM element so we can get to it later
		$(this).data('eventObject', eventObject);

		// make the event draggable using jQuery UI
		$(this).draggable({
			zIndex: 999,
			revert: true,      // will cause the event to go back to its
			revertDuration: 0  //  original position after the drag
		});
		
	});



	/* initialize the calendar
	-----------------------------------------------------------------*/

	var date = new Date();
	var d = date.getDate();
	var m = date.getMonth();
	var y = date.getFullYear();

	var eventos = new Object();
		eventos = 		[	{
			title: 'Omar Cuesta',
			start: new Date(y, m, 1),
			className: 'label-important',
			folio: '00001',
			expediente: 'AA01'
		  },
		  {
		  	title: 'Jesús Armando Guerra García',
		  	start: new Date(y, m, 2),
		  },
		  {
		  	title: 'Miguel Angel González Colin',
		  	start: new Date(y, m, 1),
		  }]

		console.log(eventos)

// {title: 'Omar Cuesta', start: new Date(y, m, 1),className: 'label-important'}

	var calendar = $('#calendar').fullCalendar({
		//isRTL: true,
		 buttonText: {
			prev: '<i class="ace-icon fa fa-chevron-left"></i>',
			next: '<i class="ace-icon fa fa-chevron-right"></i>'
		},
	
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay'
		},
		events:  eventos
		 //  {
			// title: 'Omar Cuesta',
			// start: new Date(y, m, 1),
			// className: 'label-important'
			
		 //  },
		 //  {
		 //  	title: 'Jesús Armando Guerra García',
		 //  	start: new Date(y, m, 1),
		 //  },
		 //  {
		 //  	title: 'Miguel Angel González Colin',
		 //  	start: new Date(y, m, 1),
		 //  }		  
		 
		,
		editable: false,
		droppable: false, // this allows things to be dropped onto the calendar !!!
		// drop: function(date, allDay) { // this function is called when something is dropped
		
		// 	// retrieve the dropped element's stored Event Object
		// 	var originalEventObject = $(this).data('eventObject');
		// 	var $extraEventClass = $(this).attr('data-class');
			
			
		// 	// we need to copy it, so that multiple events don't have a reference to the same object
		// 	var copiedEventObject = $.extend({}, originalEventObject);
			
		// 	// assign it the date that was reported
		// 	copiedEventObject.start = date;
		// 	copiedEventObject.allDay = allDay;
		// 	if($extraEventClass) copiedEventObject['className'] = [$extraEventClass];
			
		// 	// render the event on the calendar
		// 	// the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
		// 	$('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
			
		// 	// is the "remove after drop" checkbox checked?
		// 	if ($('#drop-remove').is(':checked')) {
		// 		// if so, remove the element from the "Draggable Events" list
		// 		$(this).remove();
		// 	}
			
		// }
		// ,
		
		selectable: true,
		selectHelper: true,
		select: function(start, end, allDay, calendar, calEvent) {
			// console.log(calEvent)
			$('.listaFichas > ul').fadeOut(150, function(){
				$('.nonemsg').fadeIn(150);
				console.log();
			});
			
			// bootbox.prompt("New Event Title:", function(title) {
			// 	if (title !== null) {
			// 		calendar.fullCalendar('renderEvent',
			// 			{
			// 				title: title,
			// 				start: start,
			// 				end: end,
			// 				allDay: allDay
			// 			},
			// 			true // make the event "stick"
			// 		);
			// 	}
			// });
			

			// calendar.fullCalendar('unselect');
		}
		,
		eventClick: function(calEvent, jsEvent, view) {
			// console.log(calEvent)
			$('.nonemsg').fadeOut(150, function(){
				$('.listaFichas > ul').fadeIn(150);
				nombrePaciente = calEvent.title;
				console.log(calEvent.title)
			});
			//display a modal
			// var modal = 
			// '<div class="modal fade">\
			//   <div class="modal-dialog">\
			//    <div class="modal-content">\
			// 	 <div class="modal-body">\
			// 	   <button type="button" class="close" data-dismiss="modal" style="margin-top:-10px;">&times;</button>\
			// 	   <form class="no-margin">\
			// 		  <label>Change event name &nbsp;</label>\
			// 		  <input class="middle" autocomplete="off" type="text" value="' + calEvent.title + '" />\
			// 		 <button type="submit" class="btn btn-sm btn-success"><i class="ace-icon fa fa-check"></i> Save</button>\
			// 	   </form>\
			// 	 </div>\
			// 	 <div class="modal-footer">\
			// 		<button type="button" class="btn btn-sm btn-danger" data-action="delete"><i class="ace-icon fa fa-trash-o"></i> Delete Event</button>\
			// 		<button type="button" class="btn btn-sm" data-dismiss="modal"><i class="ace-icon fa fa-times"></i> Cancel</button>\
			// 	 </div>\
			//   </div>\
			//  </div>\
			// </div>';
		
		
			// var modal = $(modal).appendTo('body');
			// modal.find('form').on('submit', function(ev){
			// 	ev.preventDefault();

			// 	calEvent.title = $(this).find("input[type=text]").val();
			// 	calendar.fullCalendar('updateEvent', calEvent);
			// 	modal.modal("hide");
			// });
			// modal.find('button[data-action=delete]').on('click', function() {
			// 	calendar.fullCalendar('removeEvents' , function(ev){
			// 		return (ev._id == calEvent._id);
			// 	})
			// 	modal.modal("hide");
			// });
			
			// modal.modal('show').on('hidden', function(){
			// 	modal.remove();
			// });

		}
		
	});


})


