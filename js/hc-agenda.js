jQuery(function($) {
txtTitulos();
var pacienteEdit;
var nombrePaciente;
 var mensajeAlerta;


// llenando datos

$('#tipoEstudio .item').click( showCalendar )

$('.backEstudios').click(function(){

	$('#calendar').empty('')
	$('.listaFichas ul').fadeOut();
	$('#tipoEstudio').css('display', '')
   $('.navDerecha .estudios').addClass('elementoOculto');
})
function txtTitulos (){
	$('.topBar #titulos').empty();
	$('.topBar #titulos').append('');
}
function cambiaTitulo () {
	$('.topBar #titulos').empty();
	$('.topBar #titulos').append('nombrePaciente');
	nombrePaciente = '';
}

$('.listaFichas li').click( function(){
	if ( $(this).hasClass('fichaDisabled')) {
		mensajeAlerta = "Guarde su diagnostico antes de seleccionar un nuevo paciente.";
		alertas();

		
	} else {

	$('.listaFichas li').removeClass('fichaSelect');
	$('.listaFichas li').addClass('fichaDisabled');
	$(this).addClass('fichaSelect').removeClass('fichaDisabled');
	// console.log(this);
	pacienteEdit = $(this);
	activaEditor()
	}
})

function activaEditor(){
	$('.navDerecha .estudios').addClass('elementoOculto');
	$('.datosPaciente').removeClass('elementoOculto')
	 $('#calendar').fadeOut(200, function(){
	 	$('.editor').fadeIn(200);
	 	cambiaTitulo();
	 	
	 });
}

$('#cancelar').click( cancelarEditor );

function cancelarEditor(){
	$('.navDerecha .estudios').removeClass('elementoOculto');
	$('.datosPaciente').addClass('elementoOculto')
		 $('.editor').fadeOut(200, function(){
	 	 $('#calendar').fadeIn(200);
	 	 $(pacienteEdit).removeClass('fichaSelect');
	 	 $('.listaFichas li').removeClass('fichaDisabled');
	 	 txtTitulos();
	 });

}

function alertas(){
	console.log(mensajeAlerta)
	$(".alertas .boton").html('<button>OK</button>');
	$(".alertas button").click( cierraAlerta );
	$(".alertas").fadeIn('fast');
	$(".alertas .mensaje").html(mensajeAlerta);

}

function cierraAlerta(){
		$(".alertas").fadeOut('fast', function(){
				$(".alertas .mensaje").empty('');
			});
		
}


/* initialize the external events
	-----------------------------------------------------------------*/

	$('#external-events div.external-event').each(function() {

		// create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
		// it doesn't need to have a start or end
		var eventObject = {
			title: $.trim($(this).text()) // use the element's text as the event title
		};

		// store the Event Object in the DOM element so we can get to it later
		$(this).data('eventObject', eventObject);

		// make the event draggable using jQuery UI
		$(this).draggable({
			zIndex: 999,
			revert: true,      // will cause the event to go back to its
			revertDuration: 0  //  original position after the drag
		});
		
	});



	
function showCalendar (){
	$('#calendar').removeClass('elementoOculto')
	$('#tipoEstudio').css('display', 'none')
   $('.navDerecha .estudios').removeClass('elementoOculto');
/* initialize the calendar
	-----------------------------------------------------------------*/

	var date = new Date();
	var d = date.getDate();
	var m = date.getMonth();
	var y = date.getFullYear();

	var eventos = new Object();
		eventos = 		[	{
			title: 'Omar Cuesta',
			start: new Date(y, m, 1),
			className: 'label-important',
			folio: '00001',
			expediente: 'AA01'
		  },
		  {
		  	title: 'Jesús Armando Guerra García',
		  	start: new Date(y, m, 2),
		  },
		   {
		  	title: 'Javier bermudez',
		  	start: new Date(y, m, 2),
		  },
		  {
		  	title: 'Pedro Picapiedr',
		  	start: new Date(y, m, 2),
		  },
		  {
		  	title: 'Pablo Marmol',
		  	start: new Date(y, m, 29),
		  },
		  {
		  	title: 'Pablo Marmol',
		  	start: new Date(y, m, 29),
		  },
		  {
		  	title: 'Pablo Marmol',
		  	start: new Date(y, m, 29),
		  },
		  {
		  	title: 'Pablo Marmol',
		  	start: new Date(y, m, 29),
		  },
		  {
		  	title: 'Pablo Marmol',
		  	start: new Date(y, m, 29),
		  },
		  {
		  	title: 'Pablo Marmol',
		  	start: new Date(y, m, 29),
		  },
		  {
		  	title: 'Pablo Marmol',
		  	start: new Date(y, m, 29),
		  },
		  {
		  	title: 'Pablo Marmol',
		  	start: new Date(y, m, 29),
		  },
		  {
		  	title: 'Pablo Marmol',
		  	start: new Date(y, m, 29),
		  },
		  {
		  	title: 'Pablo Marmol',
		  	start: new Date(y, m, 29),
		  },
		  {
		  	title: 'Pablo Marmol',
		  	start: new Date(y, m, 29),
		  },
		  {
		  	title: 'Pablo Marmol',
		  	start: new Date(y, m, 29),
		  },
		  {
		  	title: 'Pablo Marmol',
		  	start: new Date(y, m, 29),
		  },
		  {
		  	title: 'Pablo Marmol',
		  	start: new Date(y, m, 29),
		  },
		  {
		  	title: 'Miguel Angel González Colin',
		  	start: new Date(y, m, 10),
		  }]

		// console.log(eventos[0].start)
		// Crear funcion para determinar numero de eventos por dia
		// 

	var calendar = $('#calendar').fullCalendar({

		//isRTL: true,
		 buttonText: {
			prev: '<i class="ace-icon fa fa-chevron-left"></i>',
			next: '<i class="ace-icon fa fa-chevron-right"></i>'
		},
	
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay'
		},
		// NO MISTRAR LA LISTA DE EVENTOS
		events: eventos,
		editable: false,
		droppable: false, // this allows things to be dropped onto the calendar !!!
		selectable: false,
		selectHelper: true,
		select: function(start, end, allDay, calendar, calEvent) {
				totalEventos();

			// console.log(calEvent)
			// $('.listaFichas > ul').fadeOut(150, function(){
			// 	$('.nonemsg').fadeIn(150);
			// 	console.log();
			// });
		}
		,
		eventClick: function(calEvent, jsEvent, view) {
			// console.log(calEvent)
			$('.nonemsg').fadeOut(150, function(){
				$('.listaFichas > ul').fadeIn(150);
				nombrePaciente = calEvent.title;
				console.log(calEvent.title)
			});
			//display a modal
			// var modal = 
			// '<div class="modal fade">\
			//   <div class="modal-dialog">\
			//    <div class="modal-content">\
			// 	 <div class="modal-body">\
			// 	   <button type="button" class="close" data-dismiss="modal" style="margin-top:-10px;">&times;</button>\
			// 	   <form class="no-margin">\
			// 		  <label>Change event name &nbsp;</label>\
			// 		  <input class="middle" autocomplete="off" type="text" value="' + calEvent.title + '" />\
			// 		 <button type="submit" class="btn btn-sm btn-success"><i class="ace-icon fa fa-check"></i> Save</button>\
			// 	   </form>\
			// 	 </div>\
			// 	 <div class="modal-footer">\
			// 		<button type="button" class="btn btn-sm btn-danger" data-action="delete"><i class="ace-icon fa fa-trash-o"></i> Delete Event</button>\
			// 		<button type="button" class="btn btn-sm" data-dismiss="modal"><i class="ace-icon fa fa-times"></i> Cancel</button>\
			// 	 </div>\
			//   </div>\
			//  </div>\
			// </div>';
		
		
			// var modal = $(modal).appendTo('body');
			// modal.find('form').on('submit', function(ev){
			// 	ev.preventDefault();

			// 	calEvent.title = $(this).find("input[type=text]").val();
			// 	calendar.fullCalendar('updateEvent', calEvent);
			// 	modal.modal("hide");
			// });
			// modal.find('button[data-action=delete]').on('click', function() {
			// 	calendar.fullCalendar('removeEvents' , function(ev){
			// 		return (ev._id == calEvent._id);
			// 	})
			// 	modal.modal("hide");
			// });
			
			// modal.modal('show').on('hidden', function(){
			// 	modal.remove();
			// });

		}
		

	});

// Cambiando botones a español
	$('.fc-button-month').html('Mes');
	$('.fc-button-agendaWeek').html('Semana');
	$('.fc-button-agendaDay').html('Día');
	$('.fc-button-today').html('Hoy');




	totalEventos();
	// FUNCION PARA RRECORRE LOS EVENTOS Y AGRUPARLOS
		function totalEventos () {
			console.log(eventos)
			var fechasEventos = []
			var consultas = [];
			for (var i = 0; i < eventos.length; i++) {
				var paciente = eventos[i].title
				var fecha = eventos[i].start
				var ano = fecha.getFullYear();
				var mes = fecha.getMonth();
				var dia =fecha.getDate();
				mes = mes +1 // COMPENSANDO RETRASO DEL MES RESPECTO AL ACTUAL
					if (mes < 10) {
						mes = '0' + mes;
					}
					if (dia < 10) {
						dia = '0' + dia;
					}
				var fechaFull = ano + '-' + mes + '-' + dia;
				var consulta = {nombre:paciente, fecha:fechaFull}
				console.log(fechaFull)
				consultas.push(consulta)
				fechasEventos.push(fechaFull)
			};
			pitandoEventos(consultas, fechasEventos);
			// console.log(consultas)
		}

		
function pitandoEventos (consultas, fechas) {
	// DETERMINAR CUANTOS EVENTOS EXISTEN EL MISMO DIA CON "FECHAS"
			var count = {};
			for(var i = 0; i < fechas.length; i++){
			if(!(fechas[i] in count))count[fechas[i]] = 0;
			count[fechas[i]]++;
			// AGREGANDO LA CUENTA DE LAS CONSULTAS POR FECHA Y ESTILOS
			var eldia = $('#calendar').find('[data-date="'+ fechas[i] +'"]')
			$(eldia).addClass('diaConEventos');
			$(eldia).find('>div').find('.fc-day-content').html('<p>'+ count[fechas[i]] +'<span>/0</span><br><small>Consultas</small></p>')
			// EXTRAR NOMBRE Y DATOS DEL JSON DE LOS PACIENTES Y DIBUJARLOS EN EL HTML

			// MOSTRAR NOMBRES DE LOS PACIENTES EN LOS DIAS QUE EXISTAN CONSULTA
			$(eldia).click( function(){				
				$('.nonemsg').fadeOut(150, function(){
					$('.listaFichas').css('height', '100vh') //ponemos altura al contenedor de fichas para activar scroll
					$('.listaFichas > ul').fadeIn(150);
					nombrePaciente = consultas[0].nombre;
					});
				})
			}
			
}

} // end showCalendar
})

